<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Controller\TempController;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class TempControllerTest.
 *
 * @codeCoverageIgnore
 */
class TempControllerTest extends TestCase
{
    public function testShouldRenderHomePage()
    {
        $this->assertInstanceOf(JsonResponse::class, $this->getController()->index());
    }

    private function getController(): TempController
    {
        return new TempController();
    }
}
