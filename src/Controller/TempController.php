<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TempController.
 */
class TempController
{
    /**
     * @Route("/home", name="home")
     */
    public function index(): JsonResponse
    {
        return new JsonResponse(['code' => 200, 'message' => 'Ok'], 200);
    }
}
