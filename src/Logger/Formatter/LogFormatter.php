<?php

declare(strict_types=1);

namespace App\Logger\Formatter;


use Monolog\Formatter\FormatterInterface;

class LogFormatter implements FormatterInterface
{

    public function format(array $record)
    {
        $record = array_merge($record, $record['extra'] ?? []);
        $record['datetime'] = $record['datetime']->format('Y-m-d H:i:s');
        unset($record['extra']);

        return$record;
    }

    public function formatBatch(array $records): bool
    {
        return false;
    }
}
