<?php

declare(strict_types=1);

namespace App\Logger\Processor;

use Monolog\Processor\ProcessorInterface;

/**
 * Class VersionProcessor.
 */
class VersionProcessor implements ProcessorInterface
{
    /**
     * @var string
     */
    private $version;

    public function __construct(string $appVersion)
    {
        $this->version = $appVersion;
    }

    public function __invoke(array $record): array
    {
        list($major, $minor, $patch) = explode('.', $this->version);
        $record['extra'] = ['major' => $major, 'minor' => $minor, 'patch' => $patch];

        return $record;
    }
}
