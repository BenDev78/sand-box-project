<?php

declare(strict_types=1);

namespace App\Logger\Processor;

use Monolog\Processor\ProcessorInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ResponseProcessor.
 */
class ResponseProcessor implements ProcessorInterface
{

    public function __invoke(array $record): array
    {
        /** @var Response $response */
        $response = $record['context']['response'] ?? null;

        if (!$response instanceof Response) {
            return $record;
        }

        $record['context']['response'] = [
            'status_code' => $response->getStatusCode(),
            'version' => $response->getProtocolVersion(),
            'headers' => $this->getHeaders($response),
        ];

        return $record;
    }

    private function getHeaders(Response $response): array
    {
        $headers = [];
        foreach (['cache-control', 'date', 'x-version', 'content-type', 'expires'] as $header) {
            $headers[$header] = $response->headers->get($header);
        }

        return $headers;
    }
}
